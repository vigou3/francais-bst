# Bibliographic styles `francais` and `francaissc`

The package **francais-bst** provides the bibliography styles
`francais` and `francaissc` to typeset author-year bibliographies
using the French typographic standards outlined in "Guide de la
communication écrite au cégep, à l'université et en entreprise", by
Marie Malo (Québec Amérique, 1996).

The styles were originally generated using the program `makebst` by
Patrick W. Daly. The master bibliographic style file is heavily based
on `merlin.mbs` from the package **custom-bib**.

## Licence

LaTeX Project Public License, version 1.3c or (at your option) any
later version.

## Author

Vincent Goulet <vincent.goulet@act.ulaval.ca>

## Source code repository

https://gitlab.com/vigou3/francais-bst

## Contents

- `francais-bst.ins`: installation script; see below;
- `francais-bst.mbs`: master bibliographic style and documentation;
- `francais.dbj`, `francaissc.dbj`, `francaisbst.dbj`: driver files
  for the bibliographic styles and the language definition file;
- `francais-bst.pdf`: documentation of the styles;
- `README.md`: this file.

## Installation

The package **francais-bst** is distributed through the
Comprehensive TeX Archive Network (CTAN) and it is part of the
standard TeX distributions. Therefore, we strongly recommend using
the package manager of your distribution to install or update the
package.

TeX experts may generate the bibliographic styles and the language
definition file by running the file `francais-bst.ins` through
LaTeX:

    latex francais-bst.ins

The compilation will create the bibliographic styles (`francais.bst`,
`francaissc.bst`) and the language definition file
(`francaisbst.tex`). These files should ideally be moved into a local
or personal TeX tree as follows:

```
$TEXMFHOME/bibtex/bst/francais.bst
$TEXMFHOME/bibtex/bst/francaissc.bst
$TEXMFHOME/tex/latex/francais-bst/francaisbst.tex
```

## Documentation

The file `francais-bst.pdf` contains the documentation of the styles
and the master bibliographic style. You may recreate these documents
from the file `francais-bst.mbs` as follows:

    xelatex francais-bst.mbs
    makeindex -s gglo.ist -o francais-bst.gls francais-bst.glo
    xelatex francais-bst.mbs
    xelatex francais-bst.mbs

## Version history

The version history of the class appears in `francais-bst.pdf`.

## Comments or suggestions

The Gitlab [project repository](https://gitlab.com/vigou3/francais-bst)
is the best place to report bugs or to propose improvements to the 
project.

======================================================================

# Styles bibliographiques `francais` et `francaissc`

Le paquetage **francais-bst** contient les styles bibliographiques
`francais` et `francaissc` pour composer des bibliographies en
français conformes aux règles de présentation de la méthode
auteur-date proposées dans l'ouvrage de référence de Marie Malo,
«Guide de la communication écrite au cégep, à l'université et en
entreprise» (Québec Amérique, 1996).

Les styles ont été développés à l'origine à l'aide du programme
`makebst` de Patrick W. Daly. Le fichier maitre de styles
bibliographiques dérive étroitement du fichier `merlin.mbs` du
paquetage **custom-bib**.

## Licence

LaTeX Project Public License, version 1.3c ou (à votre choix) toute
version ultérieure.

## Auteur

Vincent Goulet <vincent.goulet@act.ulaval.ca>

## Dépôt du code source

https://gitlab.com/vigou3/francais-bst

## Contenu du paquetage

- `francais-bst.ins`: procédure d'installation; voir ci-dessous;
- `francais-bst.mbs`: fichier maitre de styles bibliographiques et
  documentation;
- `francais.dbj`, `francaissc.dbj`, `francaisbst.dbj`: scripts de
  génération des styles bibliographiques et du fichier de localisation
  français;
- `francais-bst.pdf`: documentation des styles;
- `README.md`: le présent fichier.

## Installation

Le paquetage **francais-bst** est distribué via le réseau de sites
Comprehensive TeX Archive Network (CTAN) et il fait partie des
distributions TeX standards. Par conséquent, nous recommandons
fortement d'installer ou de mettre à jour le paquetage à l'aide du
gestionnaire de paquetages de votre distribution.

Les experts TeX peuvent générer les styles bibliographiques et le
fichier de localisation français en compilant le fichier
`francais-bst.ins` avec LaTeX:

    latex francais-bst.ins

La compilation créera les styles de bibliographie (`francais.bst`,
`francaissc.bst`) et le fichier de localisation français
(`francaisbst.tex`). Idéalement, ces fichiers seront ensuite placés
une arborescence TeX locale ou personnelle comme ceci:

```
$TEXMFHOME/bibtex/bst/francais.bst
$TEXMFHOME/bibtex/bst/francaissc.bst
$TEXMFHOME/tex/latex/francais-bst/francaisbst.tex
```

## Documentation

Le fichier `francais-bst.pdf` contient la documentation des styles et
du fichier maitre de styles bibliographiques. Utilisez les commandes
suivantes pour recréer le document à partir du fichier
`francais-bst.mbs`:

    xelatex francais-bst.mbs
    makeindex -s gglo.ist -o francais-bst.gls francais-bst.glo
    xelatex francais-bst.mbs
    xelatex francais-bst.mbs

## Historique des versions

L'historique des versions se trouve dans `francais-bst.pdf`.

## Commentaires et suggestions

Le [dépôt du projet](https://gitlab.com/vigou3/francais-bst) dans
GitLab demeure le meilleur endroit pour rapporter des bogues ou pour
proposer des améliorations au projet.
