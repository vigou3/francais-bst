### -*-Makefile-*- pour préparer le paquetage francais-bst
##
## Copyright (C) 2018-2024 Vincent Goulet
##
## 'make bst' crée les styles bibliographiques et le fichier de
## mots-clés français.
##
## 'make doc' crée la documentation.
##
## 'make zip' crée l'archive du paquetage conformément aux exigences
## de CTAN.
##
## 'make release' crée une nouvelle version dans GitLab.
##
## 'make all' exécute 'bst', 'doc' et 'zip'.
##
## Auteur: Vincent Goulet
##
## Ce fichier fait partie du projet francais-bst
## http://gitlab.com/vigou3/francais-bst


## Nom du paquetage sur CTAN et principaux fichiers
PREFIX = francais
PACKAGENAME = ${PREFIX}-bst
MAIN = ${PACKAGENAME}.mbs
BSTDRIVERS = ${PREFIX}.dbj ${PREFIX}sc.dbj
LANGDRIVER = ${PREFIX}bst.dbj
README = README.md
ARCHIVE = ${PACKAGENAME}.zip
ARCHIVENOTEX = ${PACKAGENAME}-project-install.zip
SAMPLE = sample.pdf

## Nom du dépôt GitLab
REPOSURL = https://gitlab.com/vigou3/francais-bst

## Contenu du paquetage (sauf README.md qui est créé par 'make zip')
DOC = ${MAIN:.mbs=.pdf}
SOURCES = ${MAIN} ${MAIN:.mbs=.ins} ${BSTDRIVERS} ${LANGDRIVER}

## Numéro de version et date de publication extraits du fichier .mbs.
## Le résultat est une chaine de caractères de la forme "x.y
## (YYYY-MM-DD)".
VERSION = $(shell awk '/ProvidesFile/ \
	          { \
	              sub(".*\\[", "", $$2); \
	              gsub("/", "-", $$2); \
	              sub("v", "", $$3); \
	              printf("%s (%s)", $$3, $$2); \
	              exit; \
	          }' ${MAIN})

## Outils de travail
LATEX = latex -halt-on-error
XELATEX = xelatex -halt-on-error
MAKEINDEX = makeindex
TEXI2DVI = LATEX=xelatex TEXINDY=${MAKEINDEX} texi2dvi -b
CP = cp -p
RM = rm -r
MD = mkdir -p
ZIP = zip --filesync -r -9

## Répertoire temporaire pour construire l'archive
BUILDDIR := builddir

## Dépôt GitLab et authentification
REPOSNAME = $(shell basename ${REPOSURL})
APIURL = https://gitlab.com/api/v4/projects/vigou3%2F${REPOSNAME}
OAUTHTOKEN = $(shell cat ~/.gitlab/token)

## Variable automatique
TAGNAME = v$(word 1,${VERSION})


all: bst doc zip

${BSTDRIVERS:.dbj=.bst}: ${MAIN} ${BSTDRIVERS}
	${LATEX} $(word 1, ${BSTDRIVERS})
	${LATEX} $(word 2, ${BSTDRIVERS})

${LANGDRIVER:.dbj=.tex}: ${MAIN} ${LANGDRIVER}
	${LATEX} ${LANGDRIVER}

${MAIN:.mbs=.pdf}: ${MAIN}
	${XELATEX} $<
	${MAKEINDEX} -s gglo.ist -o $(addsuffix .gls,$(basename $<)) \
	  $(addsuffix .glo,$(basename $<))
	${XELATEX} $<
	${XELATEX} $<

${SAMPLE}: ${SAMPLE:.pdf=.tex} $(word 1, ${BSTDRIVERS:.dbj=.bst}) \
	${LANGDRIVER:.dbj=.tex}

${SAMPLE:.pdf=-sc.pdf}: ${SAMPLE:.pdf=-sc.tex} $(word 2, ${BSTDRIVERS:.dbj=.bst}) \
	${LANGDRIVER:.dbj=.tex}

%.pdf:
	${TEXI2DVI} $<

.PHONY: bst
bst: ${BSTDRIVERS:.dbj=.bst} ${LANGDRIVER:.dbj=.tex}

.PHONY: doc
doc: $(MAIN:.mbs=.pdf)

.PHONY: sample
sample: ${SAMPLE} ${SAMPLE:.pdf=-sc.pdf}

.PHONY: release
release: update-copyright zip check-status create-release upload create-link

.PHONY: update-copyright
update-copyright: ${MAIN} ${BSTDRIVERS} ${LANGDRIVER}
	for f in $?; \
	    do sed -E '/^(#|%)* *Copyright \(C\)/s/-20[0-9]{2}/-$(shell date "+%Y")/' \
	       $$f > $$f.tmp && \
	       ${CP} $$f.tmp $$f && \
	       ${RM} $$f.tmp; \
	done

## L'archive pour CTAN doit contenir un répertoire $PACKAGENAME
.PHONY: zip
zip: ${SOURCES} ${DOC} ${README} ${BSTDRIVERS:.dbj=.bst} ${LANGDRIVER:.dbj=.tex}
	if [ -d ${BUILDDIR} ]; then ${RM} ${BUILDDIR}; fi
	${MD} ${BUILDDIR}/${PACKAGENAME} \
	      ${BUILDDIR}/${PACKAGENAME}.tds/doc/bibtex/${PACKAGENAME} \
	      ${BUILDDIR}/${PACKAGENAME}.tds/source/bibtex/${PACKAGENAME} \
	      ${BUILDDIR}/${PACKAGENAME}.tds/bibtex/bst/${PACKAGENAME} \
	      ${BUILDDIR}/${PACKAGENAME}.tds/tex/latex/${PACKAGENAME}
	touch ${BUILDDIR}/${PACKAGENAME}/${README} && \
	  awk '(state == 0) && /^# / { state = 1 }; \
	       /^## (Author|Auteur)/ { printf("## Version\n\n%s\n\n", "${VERSION}") } \
	       state' ${README} >> ${BUILDDIR}/${PACKAGENAME}/${README}
	${CP} ${DOC} ${SOURCES} ${BUILDDIR}/${PACKAGENAME}
	${CP} ${BUILDDIR}/${PACKAGENAME}/${README} ${DOC} \
	      ${BUILDDIR}/${PACKAGENAME}.tds/doc/bibtex/${PACKAGENAME}
	${CP} ${SOURCES} \
	      ${BUILDDIR}/${PACKAGENAME}.tds/source/bibtex/${PACKAGENAME}
	${CP} ${BSTDRIVERS:.dbj=.bst} \
	      ${BUILDDIR}/${PACKAGENAME}.tds/bibtex/bst/${PACKAGENAME}
	${CP} ${LANGDRIVER:.dbj=.tex} \
	      ${BUILDDIR}/${PACKAGENAME}.tds/tex/latex/${PACKAGENAME}
	cd ${BUILDDIR}/${PACKAGENAME}.tds && \
	  ${ZIP} ../${ARCHIVE:.zip=.tds.zip} *
	cd ${BUILDDIR} && \
	  ${ZIP} ../${ARCHIVE} ${PACKAGENAME} ${ARCHIVE:.zip=.tds.zip}
	cd ${BUILDDIR}/${PACKAGENAME} && \
	   ${LATEX} ${PACKAGENAME}.ins && \
	   ${ZIP} ../../${ARCHIVENOTEX} * \
	          -x ${SOURCES} ${ARCHIVE:.zip=.tds.zip} \*.log
	${RM} ${BUILDDIR}

.PHONY: check-status
check-status:
	@{ \
	    printf "%s" "checking the status of the local repository... "; \
	    branch=$$(git branch --list | grep ^* | cut -d " " -f 2-); \
	    if [ "$${branch}" != "master"  ] && [ "$${branch}" != "main" ]; \
	    then \
	        printf "\n%s\n" "not on branch master or main"; exit 2; \
	    fi; \
	    if [ -n "$$(git status --porcelain | grep -v '^??')" ]; \
	    then \
	        printf "\n%s\n" "uncommitted changes in repository; not creating release"; exit 2; \
	    fi; \
	    if [ -n "$$(git log origin/master..HEAD | head -n1)" ]; \
	    then \
	        printf "\n%s\n" "unpushed commits in repository; pushing to origin"; \
	        git push; \
	    else \
	        printf "%s\n" "ok"; \
	    fi; \
	}

.PHONY: create-release
create-release:
	@{ \
	    printf "%s" "checking for a pre-existing release... "; \
	    http_code=$$(curl -I "${APIURL}/releases/${TAGNAME}" 2>/dev/null \
	                     | head -n1 | cut -d " " -f2) ; \
	    if [ "$${http_code}" = "200" ]; \
	    then \
	        printf "%s\n" "yes"; \
	        printf "%s\n" "-> using this release"; \
	    else \
	        printf "%s\n" "no"; \
	        printf "%s" "creating a new release in GitLab... "; \
	        name="Version ${VERSION}"; \
	        desc=$$(awk '/\\changes\{$(word 1,${VERSION})\}/ \
	            { \
	                sub(/^%[ \t]+\\changes\{.*\}\{.*\}\{/, "", $$0); \
	                out = $$0; \
	                if (match($$0, ".*\}$$")) { \
	                    out = substr($$0, RSTART, RLENGTH - 1) \
	                } else { \
	                    while (RSTART == 0) { \
	                        getline; \
	                        sub(/^%[ \t]+/, "", $$0); \
	                        if (match($$0, ".*\}$$")) { \
	                            out = out " " substr($$0, RSTART, RLENGTH - 1) \
	                        } else { \
	                            out = out " " $$0 \
	                        } \
	                    } \
	                } \
	                printf "- %s\n", out \
	            }' ${MAIN} | \
	            sed -E -e 's/\\cs\{([^\}]*)\}/`\\\1`/g' \
	                   -e 's/\\texttt\{([^\}]*)\}/`\1`/g'); \
	        curl --request POST \
	             --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	             --output /dev/null --silent \
	             "${APIURL}/repository/tags?tag_name=${TAGNAME}&ref=master" && \
	        curl --request POST \
	             --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	             --data tag_name="${TAGNAME}" \
	             --data name="$${name}" \
	             --data description="$${desc}" \
	             --output /dev/null --silent \
	             ${APIURL}/releases; \
	        printf "%s\n" "ok"; \
	    fi; \
	}

.PHONY: upload
upload:
	@printf "%s\n" "uploading the archives to the package registry..."
	for f in ${ARCHIVE} ${ARCHIVENOTEX}; \
	do \
	    curl --upload-file $${f} \
	         --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	         --silent \
	         "${APIURL}/packages/generic/${REPOSNAME}/${TAGNAME}/$${f}"; \
	done
	@printf "\n%s\n" "ok"

.PHONY: create-link
create-link:
	@printf "%s\n" "adding assets to the release..."
	$(eval PKG_ID=$(shell curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	                           --silent \
	                           "${APIURL}/packages" \
	                      | grep -o -E '\{[^{]*"version":"${TAGNAME}"[^}]*}' \
	                      | grep -o '"id":[0-9]*' | cut -d: -f 2))
	@for f in ${ARCHIVENOTEX} ${ARCHIVE}; \
	do \
	    file_id=$$(curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	                    --silent \
	                    "${APIURL}/packages/${PKG_ID}/package_files" \
	               | grep -o -E "\{[^{]*\"file_name\":\"$${f}\"[^}]*}" \
	               | grep -o '"id":[0-9]*' | cut -d: -f 2) && \
	    url="${REPOSURL:/=}/-/package_files/$${file_id}/download" && \
	    printf "  url to %s: %s\n" "$${f}" "$${url}" && \
	    curl --request POST \
	         --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	         --data name="$${f}" \
	         --data url="${REPOSURL:/=}/-/package_files/$${file_id}/download" \
	         --data link_type="package" \
	         --output /dev/null --silent \
	         "${APIURL}/releases/${TAGNAME}/assets/links"; \
	done
	@printf "%s\n" "ok"
